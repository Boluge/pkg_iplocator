<?php
/**
 * @version     1.0.0
 * @package     com_iplocator
 * @copyright   Copyright (C) 2014. Tous droits réservés.
 * @license     GNU General Public License version 2 ou version ultérieure ; Voir LICENSE.txt
 * @author      B5 Productions <stephane@b5prod.com> - http://www.b5prod.com
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controllerform');

/**
 * Country controller class.
 */
class IplocatorControllerCountry extends JControllerForm
{

    function __construct() {
        $this->view_list = 'countrys';
        parent::__construct();
    }

}