<?php
/**
 * @version     1.0.0
 * @package     com_iplocator
 * @copyright   Copyright (C) 2014. Tous droits réservés.
 * @license     GNU General Public License version 2 ou version ultérieure ; Voir LICENSE.txt
 * @author      B5 Productions <stephane@b5prod.com> - http://www.b5prod.com
 */

// No direct access
defined('_JEXEC') or die;

/**
 * Iplocator helper.
 */
class IplocatorHelper
{
	/**
	 * Configure the Linkbar.
	 */
	public static function addSubmenu($vName = '')
	{
		JSubMenuHelper::addEntry(
			JText::_('COM_IPLOCATOR_TITLE_COUNTRYS'),
			'index.php?option=com_iplocator&view=countrys',
			$vName == 'countrys'
		);
		JSubMenuHelper::addEntry(
			'Categories (Countrys)',
			"index.php?option=com_categories&extension=com_iplocator",
			$vName == 'categories.countrys'
		);

if ($vName=='categories.countrys.categorie') {
JToolBarHelper::title('IP Locator: Categories (Countrys)');
}
	}

	/**
	 * Gets a list of the actions that can be performed.
	 *
	 * @return	JObject
	 * @since	1.6
	 */
	public static function getActions()
	{
		$user	= JFactory::getUser();
		$result	= new JObject;

		$assetName = 'com_iplocator';

		$actions = array(
			'core.admin', 'core.manage', 'core.create', 'core.edit', 'core.edit.own', 'core.edit.state', 'core.delete'
		);

		foreach ($actions as $action) {
			$result->set($action, $user->authorise($action, $assetName));
		}

		return $result;
	}
}
