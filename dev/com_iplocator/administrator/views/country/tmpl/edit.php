<?php
/**
 * @version     1.0.0
 * @package     com_iplocator
 * @copyright   Copyright (C) 2014. Tous droits réservés.
 * @license     GNU General Public License version 2 ou version ultérieure ; Voir LICENSE.txt
 * @author      B5 Productions <stephane@b5prod.com> - http://www.b5prod.com
 */
// no direct access
defined('_JEXEC') or die;

JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('behavior.keepalive');
// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet('components/com_iplocator/assets/css/iplocator.css');

$pays = array("ad" => "andorre", "ae" => "emirats arabes unis", "af" => "afghanistan", "ag" => "antigua-et-barbuda", "ai" => "anguilla", "al" => "albanie", "am" => "arménie", "an" => "antilles", "ao" => "angola", "aq" => "antarctique", "ar" => "argentine", "as" => "samoa américaines", "at" => "autriche", "au" => "australie", "aw" => "aruba", "az" => "azerbaïdjan", "ba" => "bosnie-herzégovine", "bb" => "barbade", "bd" => "bangladesh", "be" => "belgique", "bg" => "bulgarie", "bh" => "bahreïn", "bi" => "burundi", "bj" => "bénin ", "bm" => "bermudes", "bn" => "brunei darussalam", "bo" => "bolivie", "br" => "brésil", "bs" => "bahamas", "bt" => "bhoutan",  "bw" => "botswana", "by" => "biélorussie", "bz" => "belize", "kh"=>"cambodge", "ca" => "canada", "cc" => "iles cocos", "cd" => "république démocratique du congo", "cf" => "république centrafricaine", "cg" => "congo", "ch" => "suisse", "ci" => "côte d'ivoire", "ck" => "iles cook", "cl" => "chili", "cm" => "cameroun", "cn" => "chine", "co" => "colombie", "cr" => "costa rica","cu" => "cuba", "cv" => "cap-vert", "cx" => "ile christmas", "cy" => "chypre", "cz" => "république tchèque", "de" => "allemagne", "dj" => "djibouti", "dk" => "danemark", "dm" => "dominique", "do" => "république dominicaine", "dz" => "algérie", "ec" => "equateur", "ee" => "estonie", "eg" => "egypte", "eh" => "sahara occidental", "er" => "erythrée", "es" => "espagne", "et" => "ethiopie", "fi" => "finlande", "fj" => "fidji", "fk" => "iles falklands", "fm" => "micronésie", "fo" => "ile feroe", "fr" => "france", "ga" => "gabon", "gd" => "grenade", "ge" => "géorgie", "gf" => "guyane française", "gh" => "ghana", "gi" => "gibraltar", "gl" => "groënland", "gq"=>"guinée équatoriale", "gm" => "gambie", "gn" => "guinée", "gp" => "guadeloupe", "gr" => "grèce", "gt" => "guatemala", "gu" => "guam", "gw" => "guinée-bissao", "gy" => "guyane", "hk" => "hong kong", "hn" => "honduras", "hr" => "croatie", "ht" => "haïti", "hu" => "hongrie", "id" => "indonésie", "ie" => "irlande", "il" => "israël", "in" => "inde", "iq" => "iraq", "ir" => "iran", "is" => "islande", "it" => "italie", "jm" => "jamaïque", "jo" => "jordanie", "jp" => "japon", "ke" => "kenya", "kg" => "kirghistan", "bf" => "burkina faso", "ki" => "kiribati", "km" => "république comorienne", "kn" => "saint-christophe-et-niévès", "kp" => "corée du nord", "kr" => "corée du sud", "kw" => "koweït", "ky" => "iles caïmans", "kz" => "kazakhstan", "la" => "laos", "lb" => "liban", "lc" => "sainte-lucie", "li" => "liechtenstein", "lk" => "sri lanka", "lr" => "libéria", "ls" => "lesotho", "lt" => "lituanie", "lu" => "luxembourg", "lv" => "lettonie", "ly" => "libye", "ma" => "maroc", "mc" => "monaco", "md" => "moldavie", "mg" => "madagascar", "ml"=>"mali", "mh" => "marshall", "mk" => "macédoine","mm"=>"myanmar", "mq"=>"martinique", "mn" => "mongolie", "mo" => "makau", "mp" => "ile mariana du nord", "mr" => "mauritanie", "ms" => "monteserrat", "mu" => "maurice", "mt"=>"malte", "mv" => "maldives", "mw" => "malawi", "mx" => "mexique west", "my" => "malaisie", "mz" => "mozambique", "na" => "namibie", "nc" => "nouvelle-calédonie", "ne" => "niger", "nf" => "ile de norfolk", "ng" => "nigeria", "ni" => "nicaragua", "nl" => "pays-bas", "no" => "norvège", "np" => "népal", "nr" => "nauru", "nu" => "niue", "nz" => "nouvelle-zélande", "om" => "oman", "pa" => "panama", "pe" => "pérou", "pf" => "polynésie française", "pg" => "papouasie - nouvelle guinée", "ph" => "philippines", "pk" => "pakistan", "pl" => "pologne", "pm" => "st. pierre and miquelon", "pn" => "pitcairn", "pr" => "porto rico", "ps" => "palestine", "pt" => "portugal", "pw" => "palau", "py" => "paraguay", "qa" => "qatar", "re" => "réunion", "ro" => "roumanie", "ru" => "fédération russe", "rw" => "rwanda", "sa" => "arabie saoudite", "sb" => "iles salomon", "sc" => "seychelles", "sd" => "soudan", "se" => "suède", "sg" => "singapour", "sh" => "saint hélène", "si" => "slovénie", "sk" => "slovaquie", "sl" => "sierra leone", "sm" => "saint-marin", "sn" => "sénégal", "so" => "somalie", "sr" => "suriname", "st" => "sao tomé-et-principe", "sv" => "salvador", "sy" => "syrie", "sz" => "swaziland", "tc" => "turks et caicos", "td" => "république du tchad", "tg" => "togo", "th" => "thaïlande", "tj" => "tchétchénie", "tk" => "iles tokelau", "tm" => "turkménistan", "tn" => "tunisie", "to" => "tonga", "tp" => "timor-oriental", "tr" => "turquie", "tt" => "trinité-et-tobago", "tv" => "tuvalu", "tw" => "taiwan", "tz" => "tanzanie", "ua" => "ukraine", "ug" => "ouganda", "gb" => "royaume-uni", "us" => "etats unis d'amérique", "uy" => "uruguay", "uz" => "ousbékistan", "va" => "vatican", "vc" => "saint-vincent-et-les grenadines", "ve" => "vénézuela", "vg" => "iles vierges américaines", "vi" => "iles vierges britanniques ", "vn" => "viêt-nam", "vu" => "vanuatu", "wf" => "wallis et futuna", "ws" => "samoa occidentales", "ye" => "yémen", "yt" => "mayotte", "yu" => "yougoslavie", "za" => "afrique du sud", "zm" => "zambie", "zw" => "zimbabwe" );
?>
<script type="text/javascript">
    function getScript(url,success) {
        var script = document.createElement('script');
        script.src = url;
        var head = document.getElementsByTagName('head')[0],
        done = false;
        // Attach handlers for all browsers
        script.onload = script.onreadystatechange = function() {
            if (!done && (!this.readyState
                || this.readyState == 'loaded'
                || this.readyState == 'complete')) {
                done = true;
                success();
                script.onload = script.onreadystatechange = null;
                head.removeChild(script);
            }
        };
        head.appendChild(script);
    }
    getScript('//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js',function() {
        js = jQuery.noConflict();
        js(document).ready(function(){


            Joomla.submitbutton = function(task)
            {
                if (task == 'country.cancel') {
                    Joomla.submitform(task, document.getElementById('country-form'));
                }
                else{

                    if (task != 'country.cancel' && document.formvalidator.isValid(document.id('country-form'))) {

                        Joomla.submitform(task, document.getElementById('country-form'));
                    }
                    else {
                        alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED')); ?>');
                    }
                }
            }
        });
    });
</script>

<form action="<?php echo JRoute::_('index.php?option=com_iplocator&layout=edit&id=' . (int) $this->item->id); ?>" method="post" enctype="multipart/form-data" name="adminForm" id="country-form" class="form-validate">
    <div class="width-60 fltlft">
        <fieldset class="adminform">
            <legend><?php echo JText::_('COM_IPLOCATOR_LEGEND_COUNTRY'); ?></legend>
            <ul class="adminformlist">

                			<li><?php echo $this->form->getLabel('id'); ?>
				<?php echo $this->form->getInput('id'); ?></li>

				<li><?php echo $this->form->getLabel('state'); ?>
				<?php echo $this->form->getInput('state'); ?></li>

				<li><?php echo $this->form->getLabel('categorie'); ?>
				<?php echo $this->form->getInput('categorie'); ?></li>

				<li><?php echo $this->form->getLabel('type'); ?>
				<?php echo $this->form->getInput('type'); ?></li>

				<li><?php echo $this->form->getLabel('pays'); ?>
				<?php //echo $this->form->getValue('pays'); ?>
                                            <select name="jform[pays]" id="jform_pays" class="required" aria-required="true" required="required" size="1">
                                                <?php foreach ($pays as $key => $value) {
                                                    $option = '<option ';
                                                    if($this->form->getValue('pays') == strtoupper ($key)){ $option .= 'selected'; }
                                                    $option.=' value="'.strtoupper ($key).'">'. ucwords ($value).'</option>';

                                                    echo $option;
                                                } ?>
                                            </select>
                                            </li>

                                                <?php // Afficher ici le liste des groupes d'utilisateurs  ?>
                                                <li><?php echo $this->form->getLabel('usergroupe'); ?>
                                                <?php echo $this->form->getInput('usergroupe'); ?></li>


				<li><?php echo $this->form->getLabel('redirect'); ?>
				<?php echo $this->form->getInput('redirect'); ?></li>
				<li><?php echo $this->form->getLabel('viewmodal'); ?>
				<?php echo $this->form->getInput('viewmodal'); ?></li>
				<li><?php echo $this->form->getLabel('modal'); ?>
				<?php echo $this->form->getInput('modal'); ?></li>
				<li><?php echo $this->form->getLabel('created_by'); ?>
				<?php echo $this->form->getInput('created_by'); ?></li>
				<li><?php echo $this->form->getLabel('update'); ?>
				<?php echo $this->form->getInput('update'); ?></li>


            </ul>
        </fieldset>
    </div>



    <input type="hidden" name="task" value="" />
    <?php echo JHtml::_('form.token'); ?>
    <div class="clr"></div>

    <style type="text/css">
        /* Temporary fix for drifting editor fields */
        .adminformlist li {
            clear: both;
        }
    </style>
</form>