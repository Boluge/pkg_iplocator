CREATE TABLE IF NOT EXISTS `#__iplocator_countrys` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`ordering` INT(11)  NOT NULL ,
`state` TINYINT(1)  NOT NULL ,
`categorie` INT(11)  NOT NULL ,
`usergroupe` INT(11)  NOT NULL ,
`type` VARCHAR(255)  NOT NULL ,
`pays` VARCHAR(2)  NOT NULL ,
`redirect` VARCHAR(255)  NOT NULL ,
`viewmodal` VARCHAR(255)  NOT NULL ,
`modal` TEXT NOT NULL ,
`checked_out` INT(11)  NOT NULL ,
`checked_out_time` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
`created_by` INT(11)  NOT NULL ,
`update` DATETIME NOT NULL ,
PRIMARY KEY (`id`)
) DEFAULT COLLATE=utf8_general_ci;

