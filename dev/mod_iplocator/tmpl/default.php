<?php
/**
 * @copyright	Copyright (c) 2014 Ip Locator. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;
	JHtml::_('behavior.keepalive');

	$component = JComponentHelper::getParams('com_iplocator');

	$jquery = $component->get('jquery');
	$local = $component->get('local');
	$dev = $component->get('dev');
	$view = $component->get('view');

	$user = JFactory::getUser();
?>
<?php
	$doc = JFactory::getDocument();
	$doc->addStyleSheet( 'modules/mod_iplocator/assets/css/style.css' );

	if($jquery  == 4){
		$doc->addScript( 'https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js' );
	} else if ($jquery  == 3){
		$doc->addScript( 'https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js' );
	} else if ($jquery  == 2){
		$doc->addScript( 'https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js' );
	} else if ($jquery  == 1){
		$doc->addScript( 'https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js' );
	}

	$doc->addScript( 'modules/mod_iplocator/assets/js/script.js' );

	jimport( 'joomla.application.module.helper' );
	jimport( 'joomla.html.parameter' );
	$module = &JModuleHelper::getModule('mod_iplocator');
	$moduleParams = new JParameter($module->params);

	// Flags
	$flag = $moduleParams->get('flag');
	$size = $moduleParams->get('flagsize');
	// Category
	$cat = $moduleParams->get('category');

	include_once(JPATH_ROOT.DS."modules".DS."mod_iplocator/assets/libs/geoipcity.inc");
	include_once(JPATH_ROOT.DS."modules".DS."mod_iplocator/assets/libs/geoipregionvars.php");

	$ip = $_SERVER['REMOTE_ADDR'];

	if( $local == '1' ){
		$json = file_get_contents('http://ip-api.com/json');
		$obj = json_decode($json);
		$ip = $obj->query;
	}

	$gi = geoip_open(realpath(JPATH_ROOT.DS."modules".DS."mod_iplocator/assets/libs/db/GeoLiteCity.dat"),GEOIP_STANDARD);

	$record = geoip_record_by_addr($gi,$ip);

	// Requete des informations présente dans la catégorie
	$db = JFactory::getDbo();
	$query = $db->getQuery(true);

	$query->select($db->quoteName(array('id', 'pays', 'type', 'redirect', 'viewmodal', 'modal', 'state')));
	$query->from($db->quoteName('#__iplocator_countrys'));
	$query->where($db->quoteName('categorie') . ' LIKE '. $db->quote($cat));
	$query->where($db->quoteName('state') . ' LIKE '. $db->quote('1'));
	$query->order('ordering ASC');

	$db->setQuery($query);
	$results = $db->loadObjectList();
?>

<?php
	if( $record ) {
		function ipmodal($content, $user){
			require_once(JPATH_ROOT.DS."modules".DS."mod_iplocator/tmpl/login.php");

			$modal = '<div id="modallocator">';
			$modal .= str_replace("'","\'", $content);
			if($user->id != 0){
				$type = 'login';
			} else {
				$type = 'logout';
			}
			$modal .= $type;

			// $modal .= '<form name="redirect" method="post">';
			// $modal .= '<p class="iplocbuttom">';
			// $modal .= '<input type="hidden"  name="redirect"  value="1">';
			// $modal .= '<a href="#" id="redirectcancel">Annuler</a> ';
			// $modal .=  '<input id="subredirect" type="submit" value="Continuer">';
			// $modal .= '</p>';
			// $modal .= '</form>';
			$modal .= '</div>';

			$modal = str_replace(CHR(13).CHR(10),"", $modal);

			return $modal;
		}

		foreach ($results as $pays) {

			$redirect = $pays->redirect;
			$code = $pays->pays;
			$url = $pays->redirect;
			$type = $pays->type;
			$viewmodal = $pays->viewmodal;
			$ctmodal = $pays->modal;

			if( $type == 1 && $viewmodal == 0 ) {
				if( $code == $record->country_code && $dev == 0 ){
					header('Location: '.$url);
					exit;
				} else if( $code == $record->country_code && $dev == 1 ){
					echo '<a href="'.$url.'" target="_blanck" >Redirection</a>';
				}
			} else if ( $type != 1 && $viewmodal == 0 ){
				if( $code != $record->country_code && $dev == 0){
					header('Location: '.$url);
					exit;
				} else if( $code == $record->country_code && $dev == 1 ){
					echo '<a href="'.$url.'" target="_blanck" >Redirection</a>';
				}
			} else if ( $type == 1 && $viewmodal == 1 ){
				if( $code == $record->country_code && $dev == 0 ){
					echo "<script type='text/javascript'>viewModal('".ipmodal($ctmodal,$user)."')</script>";
					if( isset($_POST['redirect'])) {
						header('Location: '.$url);
						exit;
					}
				} else if( $code == $record->country_code && $dev == 1 ){
					echo "<script type='text/javascript'>viewModal('".ipmodal($ctmodal,$user)."')</script>";
					if( isset($_POST['redirect'])) {
						echo '<a href="'.$url.'" target="_blanck" >Redirection</a>';
					}
				}
			} else if ( $type != 1 && $viewmodal == 1 ){
				if( $code != $record->country_code && $dev == 0 ){
					echo "<script type='text/javascript'>viewModal('".ipmodal($ctmodal,$user)."')</script>";
					if( isset($_POST['redirect'])) {
						header('Location: '.$url);
						exit;
					}
				} else if( $code != $record->country_code && $dev == 1 ){
					echo "<script type='text/javascript'>viewModal('".modal($ctmodal,$user)."')</script>";
					if( isset($_POST['redirect'])) {
						echo '<a href="'.$url.'" target="_blanck" >Redirection</a>';
					}
				}
			}
		}
	}
?>


	<?php if($view != 0): ?>
	<div id="iplocator">
		<p><b><?php echo $ip; ?></b></p>
		<?php if($record): ?>
			<p><?php echo $record->country_name; ?></p>
			<?php if( $flag != 'null' ): ?>
				<p><img src="modules/mod_iplocator/assets/img/flags/<?php echo $flag;  ?>/<?php echo $size;  ?>/<?php echo $record->country_code; ?>.png"/></p>
			<?php endif; ?>
		<?php endif; ?>
	</div>
	<?php endif; ?>
