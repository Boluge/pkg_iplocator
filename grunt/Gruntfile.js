module.exports = function(grunt) {

	require('load-grunt-tasks')(grunt);

	grunt.initConfig({

		mkdir: {
			all: {
				options: {
					create: ['git'],
					create: ['git/grunt'],
					create: ['git/dev/com_iplocator/site/language', 'git/dev/com_iplocator/administrator/language'],
					create: ['git/dev/mod_iplocator/language']
				},
			},
		},

		copy: {
			main: {
				files: [
					// Composant
					{expand: true, cwd: 'administrator/components/com_iplocator/', src: ['**'], dest: 'git/dev/com_iplocator/administrator/'},
					{expand: true, cwd: 'components/com_iplocator/', src: ['**'], dest: 'git/dev/com_iplocator/site/'},
					{expand: true, cwd: 'administrator/components/com_iplocator/', src: ['iplocator.xml'], dest: 'git/dev/com_iplocator/'},
					{expand: true, cwd: 'administrator/language/fr-FR/', src: ['fr-FR.com_iplocator.ini', 'fr-FR.com_iplocator.sys.ini'], dest: 'git/dev/com_iplocator/administrator/language/'},
					{expand: true, cwd: 'administrator/language/en-GB/', src: ['en-GB.com_iplocator.ini', 'en-GB.com_iplocator.sys.ini'], dest: 'git/dev/com_iplocator/administrator/language/'},
					{expand: true, cwd: 'language/fr-FR/', src: ['fr-FR.com_iplocator.ini','fr-FR.com_iplocator.sys.ini'], dest: 'git/dev/com_iplocator/site/language/'},
					{expand: true, cwd: 'language/en-GB/', src: ['en-GB.com_iplocator.ini','en-GB.com_iplocator.sys.ini'], dest: 'git/dev/com_iplocator/site/language/'},

					// Module
					{expand: true, cwd: 'modules/mod_iplocator/', src: ['**'], dest: 'git/dev/mod_iplocator/'},
					{expand: true, cwd: 'language/fr-FR/', src: ['fr-FR.mod_iplocator.ini','fr-FR.mod_iplocator.sys.ini'], dest: 'git/dev/mod_iplocator/language/'},

					// Grunt config
					{expand: true, cwd: '', src: ['Gruntfile.js', 'package.json'], dest: 'git/grunt/'}
				]
			}
		},

		clean: ["git/dev/com_easyform/administrator/iplocator.xml"],

		compress: {
			composant: {
				options: {
					archive: 'git/pkg_iplocator/packages/com_iplocator.zip'
				},
				files: [
					{expand: true, cwd: 'git/dev/com_iplocator/', src: ['**/*'], dest: ''}
				]
			},
			module: {
				options: {
					archive: 'git/pkg_iplocator/packages/mod_iplocator.zip'
				},
				files: [
					{expand: true, cwd: 'git/dev/mod_iplocator/', src: ['**/*'], dest: ''}
				]
			},
			pkg: {
				options: {
					archive: 'git/pkg_iplocator.zip'
				},
				files: [
					{expand: true, cwd: 'git/pkg_iplocator/', src: ['**/*'], dest: ''}
				]
			}
		},
		compass: {
			dist: {
				options: {
					sassDir: 'components/com_easyform/assets/scss',
					cssDir: 'components/com_easyform/assets/css',
					environment: 'production'
				}
			},
			dev: {
				options: {
					sassDir: 'components/com_easyform/assets/scss',
					cssDir: 'components/com_easyform/assets/css'
				}
			}
		},
		watch: {
			all: {
				files: ['administrator/components/com_easyform/**/**.**', 'components/com_easyform/**/**.**'],
				options: {
					spawn: false,
					livereload: true
				}
			},
			scss: {
				files: ['components/com_easyform/assets/scss/*.scss'],
				tasks: ['compass'],
				options: {
					spawn: false,
					livereload: true
				}
			},
			js: {
				files: ['assets/js/*.js','!assets/js/*.min.js'],
				tasks: ['jshint','uglify','concat'],
				options: {
					spawn: false,
					livereload: true
				}
			},
		}

	});

	grunt.registerTask('default', ['compass','mkdir','copy','clean','compress']);
}